package imd.ufrn.br.ciclovida;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("CicloVida", "onCreate");

        if(savedInstanceState != null)
            Log.d("CicloVida", "savedInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("CicloVida", "onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("CicloVida", "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("CicloVida", "onResume");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("CicloVida", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("CicloVida", "onDestroy");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CicloVida", "onPause");
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("CicloVida", "onConfigurationChanged");
    }

    public void inciarOutraActivity(View view) {
        Intent i = new Intent(MainActivity.this, OutraActivity.class);
        startActivity(i);
    }
}
